import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { from, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { CityService } from 'app/services/city.service';
import { FiliereService } from 'app/services/filiere.service';

@Component({
    selector     : 'register',
    templateUrl  : './register.component.html',
    styleUrls    : ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy
{
    registerForm: FormGroup;
    // Vertical Stepper
    verticalStepperStep1: FormGroup;
    verticalStepperStep2: FormGroup;
    verticalStepperStep3: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;
    private allFilieres = [];
    private years = [
      {'year': 1, 'string': '1ere'},
      {'year': 2, 'string': '2eme'}
    ];
    private cities = [];
    private institutions = [];
    private groups = [];
    private selectedInst = ''; selectedFiliere = ''; selectedYear = ''; selectedGroup = '';
    private defaultCity = 'marrakech';
    // private registrationError = false;
    // private isstudent = true;
    // private registrationSucces = false;
    private emailExists = null;
    // private loader = false;
    private filiereLoading = false;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private userService: UserService,
        private cityService: CityService,
        private filiereService: FiliereService,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.cityService.getAllCities()
        .subscribe(
            (data)=>{
            this.cities = data['cities'];
            for (let city of this.cities){
                if (city.name.toLowerCase() === this.defaultCity){
                this.verticalStepperStep3.controls['city'].setValue(city.id, {onlySelf: true});
                this.institutions = city.institutions;
                break;
                }
            }
            },
            (error)=>console.log(error)
        )

        // Vertical Stepper form stepper
        this.verticalStepperStep1 = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName : ['', Validators.required],
            email    : ['', [Validators.required, Validators.email, this.checkexistance]]
        });

        this.verticalStepperStep2 = this._formBuilder.group({
            password       : ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        this.verticalStepperStep3 = this._formBuilder.group({
            city      : ['', Validators.required],
            institution     : ['', Validators.required],
            filiere: [{value: '', disabled: true}, [Validators.required]],
            year:   [{value: '', disabled: true}, [Validators.required]],
            group:  [{value: '', disabled: true}, [Validators.required]]
        });
        

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.verticalStepperStep2.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.verticalStepperStep2.get('passwordConfirm').updateValueAndValidity();
            });

        this.verticalStepperStep3.get('city').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.institutions = this.groups = this.allFilieres =[];
                this.verticalStepperStep3.get('institution').setValue('');
                this.verticalStepperStep3.get('filiere').setValue('');
                this.verticalStepperStep3.get('year').setValue('');
                this.verticalStepperStep3.get('group').setValue('');
            });

        this.verticalStepperStep3.get('institution').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((val) => {
                this.verticalStepperStep3.get('filiere').setValue('');
                this.verticalStepperStep3.get('year').setValue('');
                this.verticalStepperStep3.get('group').setValue('');
                //console.log(this.verticalStepperStep3.controls['institution'].value)
              if(!this.verticalStepperStep3.controls['institution'].value){
                this.verticalStepperStep3.get('filiere').disable()
              }else{
                this.verticalStepperStep3.get('filiere').enable();
              }
               
            });

        this.verticalStepperStep3.get('filiere').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.verticalStepperStep3.get('year').setValue('');
              this.verticalStepperStep3.get('group').setValue('');
              this.selectedFiliere = this.verticalStepperStep3.controls['filiere'].value;
              if(!this.verticalStepperStep3.controls['filiere'].value){
                this.verticalStepperStep3.get('year').disable()
              }else{
                this.verticalStepperStep3.get('year').enable();
              }
               
            });

        this.verticalStepperStep3.get('year').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.verticalStepperStep3.get('group').setValue('');
              if(!this.verticalStepperStep3.controls['year'].value){
                this.verticalStepperStep3.get('group').disable()
              }else{
                this.verticalStepperStep3.get('group').enable();
              }
            });
    }

    setInstitutions(CityID): any{

      this.selectedInst = this.selectedYear = this.selectedGroup = this.selectedFiliere = '';
        
        for (let city of this.cities){
          if (city.id === CityID){
            this.institutions = city.institutions;
            break;
          }
        }
      }


    checkexistance: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

      if ( !control.parent || !control )
      {
          return null;
      }
    
      const email = control.parent.get('email').value;

      this.userService.emailExists(email)
      .subscribe(
              (data)=>{
                if(data['result'].exists){
                  control.setErrors({emailExists: true});
                }
              },
              (error)=>console.log(error)
            );
      return null;
    };
/*async getStat(email): Promise<any>{ //it always return a Promise
  return  await this.userService.emailExists(email).toPromise().then(result => this.emailExists = result.result);
}*/
      resetYearGroups(): void{
        this.selectedYear = this.selectedGroup = '';
      }

      reloadFilieres(instId): void{
        this.filiereLoading = true;
        this.allFilieres = [];
        // this.selectedYear = this.selectedGroup = this.selectedFiliere = '';
        if(instId) {
          this.filiereService.getFiliaireByInstitution(instId)
            .subscribe(
              (data) => {
                this.allFilieres = data['filieres'];
    
                this.filiereLoading = false;
              },
              (error) => console.log(error)
            );
        }
      }
      reloadGroups(year,filereId): void{

        this.groups = [];
        for (let f of this.allFilieres){
          // console.log(f);
          if(f.id === filereId){
            for(let g of f.groups){
              if(g.year === year){
                this.groups.push(g);
              }
            }
          }
        }
        console.log(this.groups);
      }

      confirmRegistration(): void{
        console.log(this.verticalStepperStep3.value);
        console.log('confirm');
      }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {passwordsNotMatching: true};
};

