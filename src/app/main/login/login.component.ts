import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/services/auth.service';
import { User } from 'app/classes/user';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;
    user: User;
    status = false;
    statusMessage: string;
    loader = false;
    isVisible = true;
    resetSuccess = false;
    resetError = false;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        if(this.authService.isTokenExpired() === false){
            this.router.navigate(['dashboard']);
        }
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    /************************************ */
    login(): void{
        this.status = false;
        this.loader = true;

        this.authService.login(this.loginForm.controls.email.value, this.loginForm.controls.password.value)
      .subscribe(
        (data)=> this.handlData(data),
        (error)=> this.handlError(error)
      );
        /*/console.log(this.loginForm.controls.email.value);*/
    }

    handlData(data): void{
        if(data['success'] === true){
          const usrData = data['data'].user;
          this.user = new User(usrData.email,
                               usrData.firstname,
                               usrData.lastname,
                               usrData.role.name,
                               usrData.token,
                               usrData.teacher,
                               usrData.student);
          localStorage.setItem('id_token', data.data.token);
          localStorage.setItem('user', JSON.stringify(usrData));
          localStorage.setItem('extrainfo',JSON.stringify(this.user.getExtraInfo()));
          this.status = false;
          this.statusMessage = '';
          // Update Menu
          this.updateNav(usrData.role.name);
          if(this.user.getRole() === 'admin'){
            this.router.navigate(['admin']);
          }else if(this.user.getRole() === 'teacher'){
            // Redirect
            this.router.navigate(['dashboard']);
          }else{
            this.router.navigate(['dashboard']);
          }
        }else{
          this.status = true;
          this.statusMessage = data.message;
        }
        this.loader = false;
      }
    
      handlError(error): void{
        console.log(error);
      }

      updateNav (userType : string): void{
        let newNav;
        switch(userType){
          case 'admin':
            newNav = [
              {
                  id      : 'admin',
                  title   : 'Admin',
                  type    : 'group',
                  icon    : 'apps',
                  children: [
                      {
                          id   : 'users',
                          title: 'Users',
                          type : 'item',
                          icon : 'person',
                          url  : '/apps/dashboards/analytics'
                      },
                      {
                          id   : 'payments',
                          title: 'Payments',
                          type : 'item',
                          icon : 'attach_money',
                          url  : '/apps/academy'
                      }
                  ]
              },
              {
                  id      : 'control-panel',
                  title   : 'Control Panel',
                  type    : 'group',
                  icon    : 'apps',
                  children: [
                      {
                          id   : 'cron-jobs',
                          title: 'Cron Jobs',
                          type : 'item',
                          icon : 'settings',
                          url  : '/apps/file-manager'
                      },
                      {
                          id   : 'maintenance-mode',
                          title: 'Maintenance Mode',
                          type : 'item',
                          icon : 'build',
                          url  : '/apps/todo'
                      }
                  ]
              }
          ];
          // Register the new navigation
          this._fuseNavigationService.register('new-nav', newNav);
  
          // Set the current navigation
          this._fuseNavigationService.setCurrentNavigation('new-nav');
          break;
          case 'teacher':
             newNav = [
              {
                  id      : 'tpsmgr',
                  title   : 'Gestion des TPs',
                  type    : 'group',
                  icon    : 'apps',
                  children: [
                      {
                          id   : 'tpsList',
                          title: 'Liste des TPs',
                          type : 'item',
                          icon : 'person',
                          url  : '/apps/dashboards/analytics'
                      },
                      {
                          id   : 'Ajouter un TPs',
                          title: 'Payments',
                          type : 'item',
                          icon : 'attach_money',
                          url  : '/apps/academy'
                      }
                  ]
              },
              {
                  id      : 'Gestion des stagiaires',
                  title   : 'studentsmgm',
                  type    : 'group',
                  icon    : 'apps',
                  children: [
                      {
                          id   : 'absdesig',
                          title: 'Absences',
                          type : 'item',
                          icon : 'settings',
                          url  : '/apps/file-manager'
                      },
                      {
                          id   : 'marksmgm',
                          title: 'Notes',
                          type : 'item',
                          icon : 'build',
                          url  : '/apps/todo'
                      }
                  ]
              }
          ];
          // Register the new navigation
          this._fuseNavigationService.register('new-nav', newNav);
  
          // Set the current navigation
          this._fuseNavigationService.setCurrentNavigation('new-nav');
          break;
        }
      }
}
