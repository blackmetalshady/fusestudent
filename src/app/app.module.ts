import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { DashboardModule } from 'app/main/dashboard/dashboard.module';
import { AuthGuard } from './shared/auth.guard';
import { TpsComponent } from './main/tps/tps.component';

const appRoutes: Routes = [
    {
        path    : '',
        loadChildren   : './main/login/login.module#LoginModule'
    },
    {
        path      : 'dashboard',
        loadChildren: './main/dashboard/dashboard.module#DashboardModule'
    },
    {
        path    :   'tps',
        component   :   TpsComponent
    },
    {
        path    : 'login',
        loadChildren   : './main/login/login.module#LoginModule'
    },
    {
        path    : 'register',
        loadChildren   : './main/register/register.module#RegisterModule'
    },
    {
        path    : 'tps',
        loadChildren   : './main/tps/tps.module#TpsModule'
    },
    {
        path    : 'calendar',
        loadChildren   : './main/calendar/calendar.module#CalendarModule'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        DashboardModule
    ],
    
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
