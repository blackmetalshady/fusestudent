export class User {

  constructor(private email: string,
              private firstname: string,
              private lastname: string,
              private role: string,
              private token: string,
              private teacher:string[],
              private student:string[]){}

  getEmail(){
    return this.email;
  }

  getFullname(){
    return this.firstname+' '+this.lastname;
  }

  getRole(){
    return this.role;
  }

  getExtraInfo(){
    return (!this.teacher)? this.student : this.teacher;
  }
  getToken(){
    return this.token;
  }
}
