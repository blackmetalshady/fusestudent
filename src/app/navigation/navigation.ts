import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'tps',
        title    : 'TPs',
        type     : 'group',
        children : [
            {
                id       : 'TPsList',
                title    : 'Liste des TPs',
                type     : 'item',
                icon     : 'list',
                url      : '/tps',
                badge    : {
                    title    : '2',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'calendar',
                title    : 'Calendrier',
                type     : 'item',
                icon     : 'today',
                url      : '/calendar',
                badge    : {
                    title    : '2',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'tpshistory',
                title    : 'Historique',
                type     : 'item',
                icon     : 'history',
                url      : '/tps/history'
            }
        ]
    },
    {
        id       : 'materials',
        title    : 'Cours et Tuto',
        type     : 'group',
        children : [
            {
                id       : 'materialsList',
                title    : 'Liste des Cours',
                type     : 'item',
                icon     : 'library_books',
                url      : '/material',
                badge    : {
                    title    : '2',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            }
        ]
    },
    {
        id       : 'contact',
        title    : 'Contact',
        type     : 'group',
        children : [
            {
                id       : 'contactForm',
                title    : 'Envoyer un message',
                type     : 'item',
                icon     : 'email',
                url      : '/tps',
                badge    : {
                    title    : '2',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            }
        ]
    }
];
