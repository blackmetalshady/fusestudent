import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { host } from 'app/fuse-config/index';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor( private http: HttpClient) {}
  getFiliaireByInstitution(institutionId): Observable<any>{
    return this.http.get(host+'/api/institutions/getFilieresByInstitution/'+institutionId+'.json');
  }
}
