import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { host } from 'app/fuse-config/index';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  emailExists(email): any{
    return this.http.post(host+'/api/users/exists.json', {email: email});
  }
}
