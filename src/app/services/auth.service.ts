import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { host } from '../shared/app-config';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper: JwtHelperService = new JwtHelperService();
  public logoutStatus = true;
  constructor(private http: HttpClient) { }

  login (email: string, password: string) : Observable<any>{
    return this.http.post(host+'/api/users/token.json',{email: email, password: password});
  }

  getSessionData () : Observable<any>{
    return this.http.get(host+'/api/users/userdata.json');
  }


  logout(): boolean{
    this.http.get(host+'/api/users/logout.json').subscribe(
      (data)=>{
        if(data['data']['status'] === true){
          localStorage.removeItem('id_token')
          localStorage.removeItem('user');
        }
      },
      (error)=>(error)
    )
    console.log(this.logoutStatus);
    return this.logoutStatus
  }

  isTokenExpired() : boolean{
    const token = localStorage.getItem('id_token');
    if(token){
      return this.jwtHelper.isTokenExpired(token);
    }else{
      return true;
    }
  }

  register(data) : Observable<any>{
    return this.http.post(host+'/api/users/add.json', data);
  }

  update(data) : Observable<any>{
    return this.http.post(host+'/api/users/edit.json', data);
  }

  emailExists(email) : Observable<any>{
    return this.http.post(host+'/api/users/exists.json', {email: email});
  }

  verify (userId, token) : Observable<any>{
    return this.http.post(host+'/api/users/verify.json', {user_id: userId, token: token});
  }

  forgotpassword(email) : Observable<any>{
    return this.http.post(host+'/api/users/forgotpassword.json', {email: email});
  }

  getTeachersByGroup () : Observable<any>{
    return this.http.post(host+'/api/users/getTeachersByGroup.json', {group_id: JSON.parse(localStorage.getItem('extrainfo')).group_id});
  }

  enableOrDisable (id, val) : Observable<any>{
    return this.http.post(host+'/api/users/enableOrDisable.json', {id: id, val: val});
  }

  confirmUser (id) : Observable<any>{
    return this.http.post(host+'/api/users/confirm.json', {id: id});
  }

  updatePassword(obj) : Observable<any> {
    return this.http.post(host+'/api/users/changePassword.json', obj);
  }
}
