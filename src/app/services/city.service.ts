import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { host } from 'app/shared/app-config';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(public http: HttpClient) { }

  public getAllCities(){
    return this.http.get(host+"/api/cities.json");
  }
}
